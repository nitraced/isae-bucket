\documentclass[25pt, a0paper, portrait]{tikzposter}
\usepackage[utf8]{inputenc}
 \usepackage{lmodern}
  \usepackage{amsmath,amsfonts}
 \usepackage{caption,subcaption,tikz}

\title{
\begin{tabular}{c}
Deployment and Collaborative Localization \\ of Robot Swarms Aided by UWB.
\end{tabular}
}
\author{Justin Cano}
\date{\today}
\institute{DEOS/NAVIR$^2$ES, ISAE-Supaéro \\ Département de Génie Électrique, Polytechnique Montréal/GERAD
}
 
\usepackage{blindtext}
\usepackage{comment}
\usepackage{graphicx}
\graphicspath{{fig}}
\usepackage{blindtext}
\usepackage{showframe}
\usepackage[export]{adjustbox}
\usepackage{xcolor}

\newcommand{\mbf}{\mathbf}
 
\usetheme{Wave}
\usecolorstyle[colorPalette=BlueGrayOrange]{Spain}
%\usebackgroundstyle{VerticalGradation}
%\usetitlestyle{Default}
\useblockstyle{Default}

\newcommand{\red}[1]{{\textcolor{red}{#1}}}



\begin{document}
 
\maketitle

\defineblockstyle{Justtitle}{
}{
	\ifBlockHasTitle
	\draw[color=framecolor, fill=blocktitlebgcolor,
	rounded corners=\blockroundedcorners] (blocktitle.south west)
	rectangle (blocktitle.north east);
	\fi
}


\begin{columns}
	
    \column{0.5}
       
 \useblockstyle{Justtitle}      

%\block[titlecenter]{EE}

\useblockstyle{Default}
%\vspace{-3cm}

%\begin{tikzfigure}[UWB board and Husky UGV used for the experiments.]
%\includegraphics[width=0.25\textwidth]{fig/experiments.pdf}
%\end{tikzfigure}
 

\block{Mobile Robots Localization}{

To perform their assigned tasks, Unmanned Aerial/Ground Vehicles (UAV/UGV), have to maintain \textbf{accurate} \emph{position estimates} in \textbf{real time} and \textbf{limited ressources}. Indeed, since UAVs/UGVs rely on \textit{batteries}, thus \textit{embedded calculators} have to limit their computational capacities. Moreover, the price of robotics solutions must be kept \textit{affordable} in order to maintain their relevance.

\begin{tikzfigure}[Clearpath Husky \copyright~ UGV equipped with UWB module.]
	%\centering
	\includegraphics[width=0.625\linewidth]{fig/husky_zoom}
	\label{fig:huskyzoom}
\end{tikzfigure}
}


 \block{Why Choose Ultra Wide Band?}{
	\vspace{20pt}
	Ultra-Wide Band (UWB) is a class of radio-frequency signals defined by a broad bandwith of at least $0.5~\mathrm{GHz}$.
	
	\textbf{Main advantages :}
	\begin{itemize}
		\item Good Robustness to atmospheric conditions;
		\item Computational, energetic and financial costs and calibration time reduced;
	    \item Low timestamp uncertainties;
		\item Allows precise distance measurement, with a typical STD of $0.1~\mathrm{m}$, through estimation of propagation time;
		\item Allows a good refresh rate to provide real time estimates.
	\end{itemize}
	
}


\block{Deployment Multi Robots Systems}
{
\begin{tikzfigure}[Robot Swarm Illustration]
	\includegraphics[width=0.5\linewidth,trim=4.5cm 0cm 0cm 0cm,clip]{fig/intro-swarm}
	\label{fig:intro-swarm}
\end{tikzfigure}
	
	One approach to gain \textbf{time efficiency} and \textbf{robustness} in robotic applications and is to use redundancy of multi-agents setups but this requires rigorous path planning to be set up. 
	
	Our goal is to provide {\color{blue} \textbf{deployment algorithms taking into account the precision of localization}} of each robot, allowing them to perform to the best their assigned tasks.   
	
	Considering this solution will requires the algorithms to be \textbf{distributed} : one given agent must compute its own deployment subproblem with only local information extracted from its neighborhood.
}

 

   
\column{0.5}



\block{Quantify the Network Localizability}
{
In a network formed by RF-equipped unmanned vehicles, the precision of the localization is dependent of the topology, we call this phenomenon \textbf{localizability}. Setting trajectories which maintain a good localizability is one main goal of our work. 

To evaluate the localizability of a given network configuration represented by $\mbf p$ containing all agent positions, we use statistical tools such as Cramér-Rao Lower Bound (CRLB). This mathematical tool gives a lower bound on the covariance $\Sigma$ of an unbiased position estimator $\hat{\mathbf p}$, given an observation $\mbf y$ and its probabilistic model $f(\mbf y;\mbf p)$ :
\[
\Sigma = \mathsf{cov}(\hat{\mbf p}) \succeq \left( -\mathbb{E} \left[ \frac{\partial^2 \log f(\mbf y;\mbf p)}{\partial \mbf p \partial  \mbf p^\top} \right] \right)^{-1} = \mbf B(\mbf p).
\]
The main advantage of this approach is to give an information on the quality of the configuration $\mbf p$, knowing only the observation model without prior information on the estimator structure.
}


\block{Motion Planing Maintaining Localizability}
{
A standard approach to plan motion in robotics is the \textbf{potential approach}, which requires to define an additive cost function :
\[
J(\mbf p) = J_\text{task}(\mbf p) + J_\text{avoid}(\mbf p) + J_\text{connection}(\mbf p) +  \red{J_\text{localizability}(\mbf B(\mbf p))} + ...
\]
\begin{itemize}
\item During their use, the robots have to go to specific places to perform their task which is represented by $J_\text{task}(\mbf p)$ potential;
\item $J_\text{avoid}(\mbf p)$ and $J_\text{connection}(\mbf p)$ are penalty terms to avoid collision and to maintain reasonable distances between the agents;
\item The term $\red{J_\text{localizability}(\mbf B(\mbf p))}$, will be a term to avoid wrong configuration in terms of localizability. For instance, we can set $J_\text{localizability}(\mbf p) = \text{trace}(\mbf B (\mbf p)) = TMSE(\hat{\mbf p}^*)$ where $\hat{\mbf p}^*$ is an estimator achieving the CRLB. 
\end{itemize}

For instance, we can minimize locally this potential function by descending its gradient :
\[
\mbf p^{k+1} = \mbf p^{k} - h_k \nabla_{\hat{p}}J
\]
 
}


\block{Application to a simulated case}
{
	Let a rover UGV bearing three UWB modules (tags) and three fixed transceivers (anchors) $A_i$ as shown in Fig. \ref{fig:tags}. The rover motion planner follows a potential approach including the \red{localizability term}.
	After a while, the third anchor $A_3$ will have a malfunction, leading dramatically to a strong deterioration of the position estimates of the three embedded tags. We can see in Fig. \ref{fig:mserover} the temporal evolution of the quadratic error of tags position estimates and observe the mitigation of the uncertainties and then validate our approach.
	
\begin{minipage}{0.5\linewidth}
\begin{tikzfigure}[Rover configuration]
		\centering
		\includegraphics[width=\linewidth]{fig/tags}
		\label{fig:tags}
	\end{tikzfigure}
\end{minipage}
	\begin{minipage}{0.5\linewidth}
	\begin{tikzfigure}[Evolution of $MSE(\hat{\mbf p})$ with Localizability Maint. Algo.]
		\centering
		\includegraphics[width=.9\linewidth]{fig/mseRover}
		\label{fig:mserover}
	\end{tikzfigure}
\end{minipage}


	
}


      
\block{Project Advising}
 {
 This work is co-advised between ISAE (Drs. Éric Chaumette and Gaël Pagès) and Polytechnique Montréal (Dr. Jérôme Le Ny). 
}
 

      
\end{columns}
   
   
%% Footnote
\node [above right,outer sep=0pt,minimum width=\paperwidth,align=center,fill=blue!20] at (bottomleft) 
{
\begin{minipage}[l]{0.5\linewidth}
\hspace{1.5cm}
\includegraphics[width=0.2\textwidth ]{logos/isae.png}
\end{minipage}
\begin{minipage}[r]{0.5\linewidth}
\hspace{31.5cm}
 \includegraphics[width=0.2\textwidth ]{logos/poly.png}
\end{minipage}            
};

%%
% Alternativement l'ISAE peut être au centre : mettre un \centering et pas de minipage !

\end{document}

