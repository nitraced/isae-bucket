# ISAE-Supaero Beamer/Poster Template

## Contents

I left in this repository one example of Beamer presentation and a poster template used for the DEOS PhD days. 

All are LaTeX compliant and free of use ;)

The poster and some graphics in the presentation are based on Till Tantau's TikZ library, which is just awesome in the rendering of schematics. To learn more about TikZ, if you are a baguette wielder like me, then the following reference will  perhaps help :  

<u>Reference for TikZ  :</u> (French) 

[Tikz pour l'impatient PDF notebook]: http://math.et.info.free.fr/TikZ/index.html



## Some recap to build correctly a LaTeX file with bibliography

Run once `pdflatex file.tex` to generate the `file.aux` that contains all cross-references and bibliography callback. Then the command `bibtex file.aux` will link the citations in the `.tex` proceeded in the `.aux`to the `biblio.bib` database. Finally run twice `pdflatex file.tex` one to get the citations in the document and one last time to enumerate/sort them.

Sum up : 

```sh
pdflatex file.tex
bibtex file.aux 
pdflatex file.tex
pdflatex file.tex
```



## Author

Justin Cano, PhD candidate with DEOS/NAVIR²ES (Supaero) and MRASL/DGE (Polytechnique Montréal). 

name.surname [ at ] polymtl [dot] ca